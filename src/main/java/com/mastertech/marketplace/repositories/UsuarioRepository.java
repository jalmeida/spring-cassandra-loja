package com.mastertech.marketplace.repositories;

import org.springframework.data.repository.CrudRepository;

import com.mastertech.marketplace.models.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, String> {

}
