package com.mastertech.marketplace.controllers;

import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mastertech.marketplace.models.Usuario;
import com.mastertech.marketplace.repositories.UsuarioRepository;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

	@Autowired
	UsuarioRepository usuarioRepository;
	
	@RequestMapping(method=RequestMethod.POST)
	public Usuario criar(@RequestBody Usuario usuario) {
		usuarioRepository.save(usuario);
		return usuario;
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public Iterable<Usuario> listar(){
		return usuarioRepository.findAll();
	}
	
	@RequestMapping(method=RequestMethod.POST, path="/usuario/{id}")
	public ResponseEntity<Usuario> listarPorId(UUID uuid){
		return ResponseEntity.ok().build();
	}
	
	
}
